from operator import itemgetter
from utils import to_dense

import math
import numpy as np


def get_similarity_values(data, vectorized_query, metric):
    similarity = get_metric(metric)
    size = len(vectorized_query)

    results = []
    for index in data.index:
        title = to_dense(data.at[index, 'product_title'], size)
        title = similarity(title, vectorized_query)
        description = to_dense(data.at[index, 'product_description'], size)
        description = similarity(description, vectorized_query)

        total = (title + title * 0.7 + description + description * 0.3) / 2

        product = {
            'product_uid': data.at[index, 'product_uid'],
            'similar': total,
            'file': data.at[index, 'file']
        }
        results.append(product)
    return sorted(results, key=itemgetter('similar'), reverse=True)


def get_metric(name):
    """
    This method used to return a method which calculates a similarity metric. This method used in order to be more clean
    the code in the app.py.

    :param name: Name of the method that implements a similarity metric
    :return: Method that matches the given name
    """
    metric = {
        'cosine': cosine,
        'jaccard': jaccard,
        'euclidean': euclidean,
        'manhattan': manhattan,
        'dot': dot
    }
    return metric[name]


def cosine(X, Y):

    if len(X) != len(Y):
        return 0

    dot = np.dot(X, Y)
    norma = np.linalg.norm(X)
    normb = np.linalg.norm(Y)

    if not norma or not normb:
        return 0.0

    return dot / (norma * normb)


def jaccard(X, Y):
    if len(X) != len(Y):
        return 0
    intersection = 0
    len_x = 0
    len_y = 0
    for i in range(len(X)):
        if X[i] and Y[i]:
            intersection += 1
        if X[i]:
            len_x += 1
        if Y[i]:
            len_y += 1
    union = (len_x + len_y) - intersection
    return float(intersection) / union


def euclidean(X, Y):
    if len(X) != len(Y):
        return 0
    z = 0
    for i in range(len(X)):
        if X[i] != 0 and Y[i] != 0:
            z += math.pow(X[i] - Y[i], 2)
    return math.sqrt(z)


def manhattan(X, Y):
    if len(X) != len(Y):
        return 0
    z = 0
    for i in range(len(X)):
        if X[i] != 0 and Y[i] != 0:
            z += np.abs(X[i] - Y[i])
    return z


def dot(X, Y):
    if len(X) != len(Y):
        return 0
    return sum(i[0] * i[1] for i in zip(X, Y))
