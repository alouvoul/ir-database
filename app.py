from flask import Flask, render_template, request

from preprocessing import preprocess, preprocess_query
from store import AdapterDB, FileHandling, read_data_from_csv
from metrics import get_similarity_values
from utils import get_vectorizer, csr_to_dict

app = Flask(__name__)

DELETE_DB = False

data = read_data_from_csv()
data = preprocess(data, ['product_title', 'product_description'])

tfidf_vectorizer = get_vectorizer(data)

db = AdapterDB()
if DELETE_DB:
    db.remove_all()

product_title, product_description = None, None
if not db.collection.count():
    product_description = tfidf_vectorizer.transform(data['product_description'])
    product_title = tfidf_vectorizer.transform(data["product_title"])
    uid = data.loc[:, 'product_uid'].to_numpy()

    for i in range(len(data)):
        product = {
            "product_uid": int(uid[i]),
            "product_title": csr_to_dict(product_title[i]),
            "product_description": csr_to_dict(product_description[i]),
            "file": str(uid[i]) + '.txt',
        }

        db.store(product)
    FileHandling().write_to_txt(data)
else:
    product_description = db.get_products_to_df()['product_description'].values
    product_title = db.get_products_to_df()['product_title'].values


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/results', methods=["POST"])
def find_similar_products():
    if request.method == "POST":
        query = request.form.to_dict()

        preprocessed_query = preprocess_query(query['query'])
        vectorized_query = tfidf_vectorizer.transform([preprocessed_query]).toarray()[0]

        files = AdapterDB()
        products = files.get_products_to_df()

        results = get_similarity_values(products, vectorized_query, query['metric'])
        results_with_text = FileHandling().get_descriptions(results[:int(query['number_of_results'])])

        products = []
        for i in range(int(query['number_of_results'])):
            p = {
                'file': results_with_text[i]['file'],
                'title': results_with_text[i]['title'],
                'description': results_with_text[i]['description'],
                'similar': results[i]['similar'],
            }
            products.append(p)

        for i in range(int(query['number_of_results'])):
            print("ID {product_uid} {metric} similarity: {similar:.4f}".format(product_uid=results[i]['product_uid'],
                                                                               metric=query['metric'],
                                                                               similar=results[i]['similar']))
        return render_template('results.html', products=products, query=query['query'])


@app.route('/description/<filename>')
def product_template(filename):
    title, description = FileHandling().read_txt(filename=filename)
    return render_template('description.html', title=title, description=description)


print('Start process')


if __name__ == '__main__':
    app.run(debug=True)
