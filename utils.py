from sklearn.feature_extraction.text import TfidfVectorizer
from os import path
import pickle


def get_vectorizer(data):
    file = 'vector_model.sav'

    if not path.exists(file):
        tfidf_vectorizer = TfidfVectorizer(ngram_range=(1, 1))
        tfidf_vectorizer.fit(data['product_description'])
        pickle.dump(tfidf_vectorizer, open(file, 'wb'))
    else:
        tfidf_vectorizer = pickle.load(open(file, 'rb'))

    return tfidf_vectorizer


def csr_to_dict(data):
    conv = dict()
    for i in range(len(data.data)):
        conv[str(data.indices[i])] = data.data[i]
    return conv


def to_dense(doc, size):
    dense = [0 for _ in range(size)]
    for x, y in doc.items():
        dense[int(x)] = float(y)
    return dense
