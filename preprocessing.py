from nltk.corpus import stopwords, wordnet
from nltk.tag import pos_tag
from nltk.stem.wordnet import WordNetLemmatizer
import re
import string


def preprocess(data, columns):
    """

    :param data:
    :param columns:
    :return:
    """
    # All steps for preprocessing
    for column in columns:
        for index, t in data.iterrows():
            text = data.loc[index, column]

            # Tokenize text
            text = tokenize_text(text)

            # Remove punctuations
            text = remove_punctuations(text)

            # Remove stopwords
            text = remove_stopwords(text)

            # Lemmatize tokens
            text = lemmatize(text)
            data.at[index, column] = " ".join(text)
    return data


def preprocess_query(text):
    text = tokenize_text(text)

    # Remove punctuations
    text = remove_punctuations(text)

    # Remove stopwords
    text = remove_stopwords(text)

    # Lemmatize tokens
    text = lemmatize(text)

    return " ".join(text)


def tokenize_text(text):
    if not text:
        return ""
    return [token.lower() for token in text.split(" ")]


def remove_punctuations(tokens):
    cleaned_tokens = []
    for token in tokens:
        token = re.sub("[" + string.punctuation + "]+", '', token)  # Remove punctuations
        if token:
            cleaned_tokens.append(token)
    return cleaned_tokens


def remove_stopwords(tokens):
    stopwords_english = set(stopwords.words('english'))
    return [token for token in tokens if token not in stopwords_english]


def lemmatize(tokens):
    lemmatizer = WordNetLemmatizer()
    cleaned_data = []
    for token, tag in pos_tag(tokens):
        if tag.startswith('J'):
            tag = wordnet.ADJ
        elif tag.startswith('V'):
            tag = wordnet.VERB
        elif tag.startswith('N'):
            tag = wordnet.NOUN
        elif tag.startswith('R'):
            tag = wordnet.ADV
        else:
            tag = None

        if not tag:
            cleaned_data.append(lemmatizer.lemmatize(token))
        else:
            cleaned_data.append(lemmatizer.lemmatize(token, tag))

    return cleaned_data
