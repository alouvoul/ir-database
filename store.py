from pymongo import MongoClient

import os
import pandas as pd


class AdapterDB:

    def __init__(self):
        client = MongoClient('mongodb://localhost:27017')
        db = client['home_depot']
        self.collection = db['products']

    def store(self, doc):
        self.collection.insert_one(doc)

    def remove_all(self):
        self.collection.remove({})

    def get_product(self, uid):
        return self.collection.find({'product_uid': uid})

    def get_products(self):
        return self.collection.find({})

    def get_products_to_df(self):
        data = pd.DataFrame(columns=['product_uid', 'product_title', 'product_description', 'file'])
        for product in self.get_products():
            data = data.append({
                'product_uid': product['product_uid'],
                'product_title': product['product_title'],
                'product_description': product['product_description'],
                'file': product['file']
            }, ignore_index=True)
        return data


class FileHandling:
    file_folder = os.getcwd() + "/data/products/"

    def write_to_txt(self, data):
        for i in data.index:
            file = open(self.file_folder + str(data.loc[i, 'product_uid']) + ".txt", 'w')
            file.write(data.loc[i, 'product_title'] + "\n")
            file.write(data.loc[i, 'product_description'])
            file.close()

    def read_txt(self, filename):
        file = open(self.file_folder + filename)
        title = file.readline()
        description = file.readline()

        return title, description

    def get_descriptions(self, products):
        results = []
        for product in products:
            file = open(self.file_folder + product['file'])
            title = file.readline()
            description = file.readline()
            results.append({
                'file': product['file'],
                'title': title,
                'description': description,
            })

        return results


def read_data_from_csv():
    title = pd.read_csv('./data/titles.csv', usecols=["product_uid", "product_title"],
                        encoding="ISO-8859-1").drop_duplicates(subset="product_uid", keep='first')
    description = pd.read_csv('./data/descriptions.csv', encoding="ISO-8859-1")
    data = pd.merge(title,
                    description,
                    how='left',
                    on=['product_uid', 'product_uid'])
    return data
# EOF
